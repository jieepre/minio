package com.example.minio;

import com.example.minio.utils.MinioUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class FastdfsDemoApplicationTests {
    @Autowired
    private MinioUtils minioUtils;

    @Test
    public void contextLoadsTest() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
            minioUtils.generator();
        }
        System.out.println(System.currentTimeMillis() - start);
    }
}
